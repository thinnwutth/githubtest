package login
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class loginclass {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@Given("launch GitHub web-based application")
	def launch_GitHub_webbased_application() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://github.com/')
	}

	@When("click Sign In from menu")
	def click_Sign_In_from_menu() {
		WebUI.click(findTestObject('Object Repository/Page_GitHub Lets build from here  GitHub/a_Sign in'))
	}

	@And("enter username: (.*)")
	def enter_username(String username) {
		WebUI.setText(findTestObject('Object Repository/Page_Sign in to GitHub  GitHub/input_Username or email address_login'), username)
	}

	@And("enter password: (.*)")
	def enter_password(String password) {
		WebUI.setText(findTestObject('Object Repository/Page_Sign in to GitHub  GitHub/input_Password_password'), password)
	}

	@And("click Sign In")
	def click_Sign_In() {
		WebUI.click(findTestObject('Object Repository/Page_Sign in to GitHub  GitHub/input_Password_commit'))
	}

	@Then("Login successful")
	def Login_successful() {
		WebUI.click(findTestObject('Object Repository/Page_GitHub/h2_Discover interesting projects and people_3a140f'))

		WebUI.closeBrowser()
	}
}