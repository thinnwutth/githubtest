Feature: GitHub


  Scenario Outline: Verify login functionality with valid data
    Given a valid user account
    When launch GitHub web-based application
    And click Sign In from menu
    And enter username <username>
    And enter password <password>
    And click Sign In
    Then Login successful
	Examples:
|username|password|
|thinwutthmone@yomabank.com|f+1UzUFrqtBsUATFZntgrw==|