Feature: Login


  @JIRA-YMQA-12 @JIRA-YMQA-65 @JIRA-YMQA-67 @JIRA-YMQA-69 @Automate @JIRA-YMQA-70
  Scenario Outline: Verify login functionality with valid data
    Given launch GitHub web-based application
    When click Sign In from menu
    And enter username: <username>
    And enter password: <password>
    And click Sign In
    Then Login successful

    Examples:
      | username | password |
      | thinwutthmone@yomabank.com | C@rpit19971446 |
