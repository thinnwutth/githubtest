<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Sign in</name>
   <tag></tag>
   <elementGuidId>0672d20d-9c4d-4087-ada9-0afb974a355b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.HeaderMenu-link.HeaderMenu-link--sign-in.flex-shrink-0.no-underline.d-block.d-lg-inline-block.border.border-lg-0.rounded.rounded-lg-0.p-2.p-lg-0</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Sign in')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>5cf356f7-d5b3-4b98-9357-ca77c61ae1b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/login</value>
      <webElementGuid>5a6ef61b-ec6e-42d8-8d82-0729f11a78d3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>HeaderMenu-link HeaderMenu-link--sign-in flex-shrink-0 no-underline d-block d-lg-inline-block border border-lg-0 rounded rounded-lg-0 p-2 p-lg-0</value>
      <webElementGuid>dd900326-8616-4f76-b293-ea837cc1a69a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-hydro-click</name>
      <type>Main</type>
      <value>{&quot;event_type&quot;:&quot;authentication.click&quot;,&quot;payload&quot;:{&quot;location_in_page&quot;:&quot;site header menu&quot;,&quot;repository_id&quot;:null,&quot;auth_type&quot;:&quot;SIGN_UP&quot;,&quot;originating_url&quot;:&quot;https://github.com/&quot;,&quot;user_id&quot;:null}}</value>
      <webElementGuid>a2364923-e562-4073-b273-e52f45ce3e8f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-hydro-click-hmac</name>
      <type>Main</type>
      <value>cd4f672ed9a2fa51ea92c28de162e81edb2d11a2aad6884ec89a6d60b21b1cfb</value>
      <webElementGuid>f211a67c-ae5d-4723-bcce-2f2eea9fecaf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-ga-click</name>
      <type>Main</type>
      <value>(Logged out) Header, clicked Sign in, text:sign-in</value>
      <webElementGuid>bb21bbca-19b5-4526-a2b9-d11b06da35cd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
              Sign in
            </value>
      <webElementGuid>59a602a3-27f0-4932-8066-a8c2ec9027eb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;logged-out env-production page-responsive header-overlay home-campaign intent-mouse&quot;]/div[@class=&quot;logged-out env-production page-responsive header-overlay home-campaign&quot;]/div[@class=&quot;position-relative js-header-wrapper&quot;]/header[@class=&quot;Header-old header-logged-out js-details-container Details position-relative f4 py-3&quot;]/div[@class=&quot;container-xl d-flex flex-column flex-lg-row flex-items-center p-responsive height-full position-relative z-1&quot;]/div[@class=&quot;HeaderMenu--logged-out p-responsive height-fit position-lg-relative d-lg-flex flex-column flex-auto pt-7 pb-4 top-0&quot;]/div[@class=&quot;header-menu-wrapper d-flex flex-column flex-self-end flex-lg-row flex-justify-between flex-auto p-3 p-lg-0 rounded rounded-lg-0 mt-3 mt-lg-0&quot;]/div[@class=&quot;d-lg-flex flex-items-center px-3 px-lg-0 mb-3 mb-lg-0 text-center text-lg-left&quot;]/div[@class=&quot;position-relative mr-lg-3 d-lg-inline-block&quot;]/a[@class=&quot;HeaderMenu-link HeaderMenu-link--sign-in flex-shrink-0 no-underline d-block d-lg-inline-block border border-lg-0 rounded rounded-lg-0 p-2 p-lg-0&quot;]</value>
      <webElementGuid>eff9ca0c-eff2-4764-98a5-14ab29a3974f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Sign in')]</value>
      <webElementGuid>7249f1b9-4dfb-496b-851a-1a68d623ad19</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='↵'])[8]/following::a[1]</value>
      <webElementGuid>e9ac4859-83b7-4888-9c6f-f3f8432e8077</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='All GitHub'])[4]/following::a[1]</value>
      <webElementGuid>fc093c51-a7c8-450c-abc7-4f43795177f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign up'])[1]/preceding::a[1]</value>
      <webElementGuid>f9e58e39-667c-40ea-87fd-40a9edd0fea8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Let’s build from here'])[1]/preceding::a[2]</value>
      <webElementGuid>f3294a9d-784e-4677-8667-49778a2161e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Sign in']/parent::*</value>
      <webElementGuid>49f3a512-b551-45c6-8dae-3a8fd3168c52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/login')]</value>
      <webElementGuid>ea5c910f-c531-4483-9ff7-30a3d12c21f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/a</value>
      <webElementGuid>5e0229dc-6c4a-4391-b919-15821fff3c04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/login' and (text() = '
              Sign in
            ' or . = '
              Sign in
            ')]</value>
      <webElementGuid>ad33b342-c766-489e-9232-0836f560556e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
