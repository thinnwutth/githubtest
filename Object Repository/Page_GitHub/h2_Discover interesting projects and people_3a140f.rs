<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_Discover interesting projects and people_3a140f</name>
   <tag></tag>
   <elementGuidId>dc374f8d-bdf7-45a6-9dc0-a72e34a7a427</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>h2.h2.lh-condensed.mb-2</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='panel-1']/div/h2</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
      <webElementGuid>882fe4f1-a183-4c5c-9e79-63282b02daf3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>h2 lh-condensed mb-2</value>
      <webElementGuid>adb2855b-7bd2-4bc6-b9a1-206fbec07034</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Discover interesting projects and people to populate your personal news feed.</value>
      <webElementGuid>67f7da36-0110-476c-bbce-50142118daf0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;panel-1&quot;)/div[@class=&quot;Box p-5 mt-3&quot;]/h2[@class=&quot;h2 lh-condensed mb-2&quot;]</value>
      <webElementGuid>baa77fdd-7b72-4bec-9aa2-f4a829abb006</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='panel-1']/div/h2</value>
      <webElementGuid>788a91e6-a08f-4838-82e8-f95d6898f848</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply changes'])[1]/following::h2[1]</value>
      <webElementGuid>e8e4640a-154f-4833-a7c6-bd972e508313</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommendations'])[1]/following::h2[1]</value>
      <webElementGuid>19c2f917-8ec5-4b46-aaf7-93f566e76a4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='watch'])[1]/preceding::h2[1]</value>
      <webElementGuid>e99e2112-cb9e-40a4-a27c-a07a2a1752dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='star'])[1]/preceding::h2[1]</value>
      <webElementGuid>24cf08d6-c577-4fac-9e47-362081703976</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Discover interesting projects and people to populate your personal news feed.']/parent::*</value>
      <webElementGuid>872fe5f4-75f0-4293-8769-a0d87cc45559</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/h2</value>
      <webElementGuid>94325c91-59e6-4fbe-b0c2-cf2bd71a980f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h2[(text() = 'Discover interesting projects and people to populate your personal news feed.' or . = 'Discover interesting projects and people to populate your personal news feed.')]</value>
      <webElementGuid>bcfcee04-9768-4d04-a2d1-46857d953f45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
